#define CUTOFF 1000

void inssort(double *a, int n) {
    int i, j;
    double t;

    for(i = 1; i < n; i++) {
        j = i;
        while((j> 0) && (a[j - 1] > a[j])) {
            t = a[j - 1];
            a[j - 1] = a[j];
            a[j] = t;
            j--;
        }
    }
}

int partition(double *a, int n) {
    int i, j;
    double p, t;
    int first = 0;
    int mid = n/2;
    int last = n-1;

    if(a[first] > a[mid]) {
        t = a[first];
        a[first] = a[mid];
        a[mid] = t;
    }

    if(a[mid] > a[last]) {
        t = a[mid];
        a[mid] = a[last];
        a[last] = t;
    }

    if(a[first] > a[mid]) {
        t = a[first];
        a[first] = a[mid];
        a[mid] = t;
    }

    p = a[mid];
    for(i = 1, j = n - 2; /**/ ; i++, j--) {
        while(a[i] < p) {
            i++;
        }

        while(a[j] > p) {
            j--;
        }

        if(i >= j) {
            break;
        }

        t = a[i];
        a[i] = a[j];
        a[j] = t;
    }

    return i;
}

void quicksort(double *a, int n) {
    int i;

    if(n <= CUTOFF) {
        inssort(a, n);
        return;
    }

    i = partition(a, n);

    quicksort(a, i);
    quicksort(a + i, n - i);
}
